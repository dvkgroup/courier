package geo

import (
	"os"
	"regexp"
	"strconv"
	"strings"
)

func ParsePolygonJS(file string) (map[string][]Point, error) {
	//data, err := os.ReadFile("./public/js/polygon.js")
	data, err := os.ReadFile(file)
	if err != nil {
		return nil, err
	}
	// делаем файл строкой и удаляем переносы
	str := strings.Replace(string(data), "\n", "", -1)

	// регулярка для поиска переменной с массивом координат
	regexPolygonVarsRaw := regexp.MustCompile(`\s*var\s*(\w+)\s*=\s*\[(\s*\[.+?],];\s*)+`)
	// регулярка для одной точки
	regexPolygonPointsRaw := regexp.MustCompile(`\d+.\d+,\s*\d+.\d+`)

	// ищем переменные
	varsRaw := regexPolygonVarsRaw.FindAllStringSubmatch(str, -1)

	// ключ - название переменной, значение - массив точек
	vars := make(map[string][]Point)

	for _, v := range varsRaw {
		// v[1] название переменной, v[2] массив точек, пока еще с лишними скобками
		if len(v) < 2 {
			continue
		}

		// ищем точки
		pointsRaw := regexPolygonPointsRaw.FindAllStringSubmatch(v[2], -1)

		for _, p := range pointsRaw {
			// убираем лишнее и разделяем на координаты
			pointStr := strings.Split(strings.Replace(p[0], " ", "", -1), ",")

			// широта
			lat, err := strconv.ParseFloat(pointStr[0], 64)
			if err != nil {
				return nil, err
			}
			// долгота
			lng, err := strconv.ParseFloat(pointStr[1], 64)
			if err != nil {
				return nil, err
			}

			point := Point{lat, lng}

			vars[v[1]] = append(vars[v[1]], point)
		}
	}

	return vars, nil
}
