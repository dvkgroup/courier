package storage

import (
	"context"
	"encoding/json"
	"github.com/redis/go-redis/v9"
	"gitlab.com/dvkgroup/courier/module/courier/models"
)

const (
	courier_store = "couriers"
)

//go:generate go run github.com/vektra/mockery/v2@v2.20.0 --name CourierStorager
type CourierStorager interface {
	Save(ctx context.Context, courier models.Courier) error // сохранить курьера по ключу courier
	GetOne(ctx context.Context) (*models.Courier, error)    // получить курьера по ключу courier
}

type CourierStorage struct {
	storage *redis.Client
}

func NewCourierStorage(storage *redis.Client) CourierStorager {
	return &CourierStorage{storage: storage}
}

func (c CourierStorage) Save(ctx context.Context, courier models.Courier) error {
	data, err := json.Marshal(courier)
	if err != nil {
		return err
	}

	err = c.storage.Set(ctx, courier_store, data, 0).Err()
	if err != nil {
		return err
	}

	return nil
}

func (c CourierStorage) GetOne(ctx context.Context) (*models.Courier, error) {
	cJson, err := c.storage.Get(ctx, courier_store).Result()
	if err != nil {
		return nil, err
	}

	var courier models.Courier
	err = json.Unmarshal([]byte(cJson), &courier)
	if err != nil {
		return nil, err
	}

	return &courier, nil
}
