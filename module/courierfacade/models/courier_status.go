package models

import (
	cm "gitlab.com/dvkgroup/courier/module/courier/models"
	om "gitlab.com/dvkgroup/courier/module/order/models"
)

type CourierStatus struct {
	Courier cm.Courier `json:"courier"`
	Orders  []om.Order `json:"orders"`
}
