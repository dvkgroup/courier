package cache

import (
	redis "github.com/redis/go-redis/v9"
)

func NewRedisClient(host, port string) *redis.Client {
	// реализуйте создание клиента для Redis
	client := redis.NewClient(&redis.Options{
		Addr: host + ":" + port,
	})

	return client
}
