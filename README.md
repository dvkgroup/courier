# Courier

### Запуск
```bash
docker compose up
```

### Links
- [Courier](http://localhost:8080/)
- [Prometheus](http://localhost:9090/)
- [Grafana](http://localhost:3000/) (user: admin pass: admin)