package run

import (
	"context"
	"github.com/gin-gonic/gin"
	"gitlab.com/dvkgroup/courier/cache"
	"gitlab.com/dvkgroup/courier/geo"
	metrics2 "gitlab.com/dvkgroup/courier/metrics"
	cservice "gitlab.com/dvkgroup/courier/module/courier/service"
	cstorage "gitlab.com/dvkgroup/courier/module/courier/storage"
	"gitlab.com/dvkgroup/courier/module/courierfacade/controller"
	cfservice "gitlab.com/dvkgroup/courier/module/courierfacade/service"
	oservice "gitlab.com/dvkgroup/courier/module/order/service"
	ostorage "gitlab.com/dvkgroup/courier/module/order/storage"
	"gitlab.com/dvkgroup/courier/router"
	"gitlab.com/dvkgroup/courier/server"
	"gitlab.com/dvkgroup/courier/workers/order"
	"net/http"
	"os"
	"time"
)

type App struct {
}

func NewApp() *App {
	return &App{}
}

func (a *App) Run() error {
	// получение хоста и порта redis
	host := os.Getenv("REDIS_HOST")
	port := os.Getenv("REDIS_PORT")

	// инициализация клиента redis
	rclient := cache.NewRedisClient(host, port)

	// инициализация контекста с таймаутом
	ctx, cancel := context.WithTimeout(context.Background(), 100*time.Millisecond)
	defer cancel()

	// проверка доступности redis
	_, err := rclient.Ping(ctx).Result()
	if err != nil {
		return err
	}

	// парсим polygon.js
	polygons, err := geo.ParsePolygonJS("./public/js/polygon.js")
	if err != nil {
		return err
	}

	// инициализация разрешенной зоны
	allowedZone := geo.NewAllowedZone(polygons["mainPolygon"])
	// инициализация запрещенных зон
	disAllowedZones := []geo.PolygonChecker{geo.NewDisAllowedZone1(polygons["noOrdersPolygon1"]),
		geo.NewDisAllowedZone2(polygons["noOrdersPolygon2"])}

	// инициализация метрик
	metrics := metrics2.NewMetrics()

	// инициализация хранилища заказов
	orderStorage := ostorage.NewOrderStorage(rclient)
	// инициализация сервиса заказов
	orderService := oservice.NewOrderService(orderStorage, allowedZone, disAllowedZones)

	orderGenerator := order.NewOrderGenerator(orderService)
	orderGenerator.Run()

	oldOrderCleaner := order.NewOrderCleaner(orderService)
	oldOrderCleaner.Run()

	// инициализация хранилища курьеров
	courierStorage := cstorage.NewCourierStorage(rclient)
	// инициализация сервиса курьеров
	courierSevice := cservice.NewCourierService(courierStorage, allowedZone, disAllowedZones)

	// инициализация фасада сервиса курьеров
	courierFacade := cfservice.NewCourierFacade(courierSevice, orderService)

	// инициализация контроллера курьеров
	courierController := controller.NewCourierController(courierFacade, metrics)

	// инициализация роутера
	routes := router.NewRouter(courierController)
	// инициализация сервера
	r := server.NewHTTPServer()
	// инициализация группы роутов
	api := r.Group("/api")
	// инициализация роутов
	routes.CourierAPI(api)

	mainRoute := r.Group("/")

	routes.Swagger(mainRoute)
	routes.Prometheus(mainRoute)
	// инициализация статических файлов
	r.NoRoute(gin.WrapH(http.FileServer(http.Dir("public"))))

	// запуск сервера
	//serverPort := os.Getenv("SERVER_PORT")

	if os.Getenv("ENV") == "prod" {
		certFile := "/app/certs/cert.pem"
		keyFile := "/app/certs/private.pem"
		return r.RunTLS(":443", certFile, keyFile)
	}

	return r.Run()
}
